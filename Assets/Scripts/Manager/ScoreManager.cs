﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utilities;

public class ScoreManager : MonoSingleton<ScoreManager>
{
   [SerializeField] private TextMeshProUGUI scoreTxt;
   private static ScoreManager instance;

   private void Awake()
   {
      if (instance == null)
      {
         instance = this;
      }
      else
      {
         Destroy(gameObject);
      }
   }

   public static ScoreManager MyInstance
   {
      get
      {
         if (instance == null)
         {
            instance = new ScoreManager();
         }

         return instance;
      }
   }

   public void ScoreUI(int _score)
   {
      scoreTxt.text = "Score: " + _score;
   }
}
