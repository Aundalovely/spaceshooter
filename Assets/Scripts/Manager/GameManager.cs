﻿using System;
using SpaceShip;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private GameOver gameOver;
        [SerializeField] private PlayerSpaceShip playerSpaceShip;
        [SerializeField] private EnemySpaceShip enemySpaceShip;
        [SerializeField] private ScoreManager scoreManager;
        [SerializeField] private GameObject meteor;
        [SerializeField] private int playerSpaceShipHp;
        [SerializeField] private int playerSpaceShipMoveSpeed;
        [SerializeField] private int enemySpaceShipHp;
        [SerializeField] private int enemySpaceShipMoveSpeed;
        [SerializeField] private float randomRangeOne;
        [SerializeField] private float randomRangeTwo;
        [SerializeField] private int scoreValue = 1;
        
        private int collectedScore;
        private static GameManager instance;


        private void Awake()
        {
            Debug.Assert(gameOver != null, "gameOver cannot be null");
            Debug.Assert(playerSpaceShip != null, "playerSpaceShip cannot be null");
            Debug.Assert(enemySpaceShip != null, "enemySpaceShip cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceShipHp > 0, "playerSpaceShipHp hp has to be more than zero");
            Debug.Assert(playerSpaceShipMoveSpeed > 0, "playerSpaceShipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceShipHp > 0, "enemySpaceShipHp has to be more than zero");
            Debug.Assert(enemySpaceShipMoveSpeed > 0, "enemySpaceShipMoveSpeed has to be more than zero");
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DestroyImmediate(gameObject);
            }
        }
        public static GameManager MyInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameManager();
                }

                return instance;
            }
        }


        private void Start()
        {
            InvokeRepeating("SpawnEnemySpaceship" ,1f,1f);
            StartGame();
            ScoreManager.MyInstance.ScoreUI(collectedScore);
        }

        private void StartGame()
        {
            SpawnPlayerSpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceShip);
            spaceship.Init(playerSpaceShipHp, playerSpaceShipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;

        }

        public void OnPlayerSpaceshipExploded()
        {
            gameOver.Setup();
        }

        private void SpawnEnemySpaceship()
        {
            float getRandomOne = UnityEngine.Random.Range(randomRangeOne,randomRangeTwo);
            float getRandomTwo = UnityEngine.Random.Range(0,2);
            if (getRandomTwo == 1)
            {
                var spaceship = Instantiate(enemySpaceShip,new Vector3(getRandomOne, enemySpaceShip.transform.position.y, 0), Quaternion.identity);
                spaceship.Init(enemySpaceShipHp, enemySpaceShipMoveSpeed);
                spaceship.OnExploded += OnEnemySpaceshipExploded;
                Instantiate(meteor, new Vector3(getRandomOne, meteor.transform.position.y, 0), Quaternion.identity);
            }
        }

        private void OnEnemySpaceshipExploded()
        {
            MyInstance.AddScore(scoreValue);
        }
        
        public void AddScore(int _score)
        {
            collectedScore += _score;
            ScoreManager.MyInstance.ScoreUI(collectedScore);
        }
        
        

        
        
    
    }
}
