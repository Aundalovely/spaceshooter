﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class GameOver : MonoBehaviour
    {
        public void Setup()
        {
            gameObject.SetActive(true);
        }

        public void RestartButton()
        {
            SceneManager.LoadScene("LevelOne");
        }

        public void MenuButton()
        {
            SceneManager.LoadScene("MainMenu");
        }

        public void ExitButton()
        {
            Application.Quit();
        }
    
    }
}
