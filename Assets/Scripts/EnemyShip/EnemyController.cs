﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SpaceShip;
using UnityEngine;
using UnityEngine.Serialization;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D player;
        [SerializeField] private float movementForce;
        [SerializeField] private EnemySpaceShip enemySpaceShip;

        private void Start()
        {
            player.velocity = new Vector2(0,-movementForce*Time.fixedDeltaTime);
        }

        private void Update()
        {
            enemySpaceShip.Fire();
        }

        private void MoveToPlayer()
        {
            //Todo: Implement this later
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Detection")
            {
                Destroy(gameObject);
            }
        }
    }    
}

