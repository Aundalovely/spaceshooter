﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementMeteor : MonoBehaviour
{
    [SerializeField] private Rigidbody2D player;
    [SerializeField] private float movementForce;
    
    private void Start()
    {
        player.velocity = new Vector2(0,-movementForce*Time.fixedDeltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Detection")
        {
            Destroy(gameObject);
        }
    }
}
