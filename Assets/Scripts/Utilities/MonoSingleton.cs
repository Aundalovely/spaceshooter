﻿using UnityEngine;

namespace Utilities
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static bool isShuttingDown = false;
        private static object lockObject = new object();
        private static T instance;
        
        public static T Instance
        {
            get
            {
                if (isShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                                     "' already destroyed. Returning null.");
                    return null;
                }
 
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        // Search for existing instance.
                        instance = (T)FindObjectOfType(typeof(T));
 
                        // Create new instance if one doesn't already exist.
                        if (instance == null)
                        {
                            // Need to create a new GameObject to attach the singleton to.
                            var singletonObject = new GameObject();
                            instance = singletonObject.AddComponent<T>();
                            singletonObject.name = typeof(T).ToString() + " (Singleton)";
 
                            // Make instance persistent.
                        }
                    }
 
                    return instance;
                }
            }
        }
        
        protected void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(gameObject);
            }
            
            DontDestroyOnLoad(gameObject);
        }
 
 
        private void OnApplicationQuit()
        {
            isShuttingDown = true;
        }
 
 
        private void OnDestroy()
        {
            isShuttingDown = true;
        }
    }
}
